#include <iostream>
#include <string>
#include <cstdlib>
#include <conio.h>

// Class Headers
#include "GameSystem.h"

using namespace std;

int main(){
	GameSystem gameSystem("level1.txt");
	gameSystem.playGame();
}