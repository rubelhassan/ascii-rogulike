#pragma once
class Player
{
public:
	Player();
	~Player();
	void init(int level, int health, int attack, int defence, int experience);

	void setPosition(int x, int y);
	void getPosition(int &x, int &y);
	int attack();
	void addExperience(int val);
	int takeDamage(int attack);
private:
	// properties
	int _level;
	int _health;
	int _attack;
	int _defence;
	int _experience;
	// position
	int _x;
	int _y;

};

