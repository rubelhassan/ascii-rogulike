#include "Level.h"
#include <fstream>
#include <iostream>
#include <cstring>
#include <string>

using namespace std;

Level::Level(){
}


Level::~Level(){
}

void Level::load(string fileName, Player &player){
	// load the level
	ifstream file;
	file.open(fileName);
	if (file.fail()){
		perror(fileName.c_str());
		system("PAUSE");
		exit(1);
	}
	string line;
	while (getline(file, line)){
		_levelData.push_back(line);
	}
	file.close();

	// proccess the level
	char tile;
	for (unsigned i = 0; i < _levelData.size(); i++){
		for (unsigned j = 0; j < _levelData[i].size(); j++){
			tile = _levelData[i][j];
			
			switch (tile){
			case '@': // player
				player.setPosition(j,i);
				break; 
			case 'S': // snake
				_enemies.push_back(Enemy("Snake", tile, 1, 3, 1, 10, 50));
				_enemies.back().setPosition(j, i);
				break;
			case 'g': // goblin
				_enemies.push_back(Enemy("Goblin", tile, 2, 10, 5, 35, 150));
				_enemies.back().setPosition(j, i);
				break;
			case 'O': // ogre
				_enemies.push_back(Enemy("Ogre", tile, 4, 20, 40, 200, 500));
				_enemies.back().setPosition(j, i);
				break;
			case 'D': // Dragon
				_enemies.push_back(Enemy("Dragon", tile, 100, 2000, 2000, 20000, 5000));
				_enemies.back().setPosition(j, i);
				break;
			case 'B': // bandit
				_enemies.push_back(Enemy("Bandit", tile, 3, 15, 10, 100, 250));
				_enemies.back().setPosition(j, i);
				break;
			default:
				break;
			}
		}
	}
}
void Level::print(){
	cout << string(100, '\n') << endl;
	for each (auto lev in _levelData){
		// cout << lev << endl;
		printf("%s\n", lev.c_str());
	}
	printf("\n");
}

char Level::getTile(int x, int y){
	return _levelData[y][x];
}

void Level::setTile(int x, int y, char tile){
	_levelData[y][x] = tile;
}

void Level::processPlayerMove(Player &player, int targetX, int targetY){
	int playerX;
	int playerY;
	player.getPosition(playerX, playerY);
	char moveTile = getTile(targetX, targetY);
	switch (moveTile){
	case '.':
		player.setPosition(targetX, targetY);
		setTile(playerX, playerY, '.');
		setTile(targetX, targetY, '@');
		break;
	case '#':
		printf("You can't run into a wall!");
		system("PAUSE >nul ");
		break;
	default:
		battleMonster(player, targetX, targetY);
		break;
	}
}

void Level::movePlayer(char input, Player &player){

	int playerX;
	int playerY;
	player.getPosition(playerX, playerY);

	switch (input){
	case 'w': // up
	case 'W':
		processPlayerMove(player, playerX, playerY - 1);
		break;
	case 's': // down
	case 'S':
		processPlayerMove(player, playerX, playerY + 1);
		break;
	case 'a': // left
	case 'A':
		processPlayerMove(player, playerX - 1, playerY);
		break;
	case 'd': // right
	case 'D':
		processPlayerMove(player, playerX + 1, playerY);
		break;
	default:
		cout << "Invalid Input!" << endl;
		system("PAUSE");
		break;
	}
}

void Level::battleMonster(Player &player, int targetX, int targetY){
	int enemyX, enemyY, attackRoll, attackResult;
	int playerX;
	int playerY;
	player.getPosition(playerX, playerY);
	string enemyName;
	for (unsigned i = 0; i < _enemies.size(); i++){
		_enemies[i].getPosition(enemyX, enemyY);
		enemyName = _enemies[i].getName();
		if (targetX == enemyX && targetY == enemyY){

			// Battle! player's attack
			attackRoll = player.attack();
			printf("\nYou attacked %s with a roll of %d\n",enemyName.c_str(), attackRoll);
			attackResult = _enemies[i].takeDamage(attackRoll);
			if (attackResult != 0){
				setTile(targetX, targetY, '.');
				print();
				printf("Monster died!\n");
				printf("Level Up!\n");
				player.addExperience(attackResult);
				system("pause");
				return;
			}
			// Monster's attack
			attackRoll = _enemies[i].attack();
			printf("%s attacked you with a roll of %d\n", enemyName.c_str(), attackRoll);
			attackResult = player.takeDamage(attackRoll);
			if (attackResult == 1){
				setTile(playerX, playerY, 'X');
				print();
				printf("You died!");
				system("pause");

				exit(0);
			}
			system("pause");
			return;
		}
	}
}