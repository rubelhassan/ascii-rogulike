#include "Player.h"
#include<random>
#include<ctime>
using namespace std;
Player::Player(){
	_x = 0;
	_y = 0;
}


Player::~Player()
{
}

void Player::init(int level, int health, int attack, int defence, int experience){
	_level = level;
	_health = health;
	_attack = attack;
	_defence = defence;
	_experience = experience;
}

void Player::setPosition(int x, int y){
	_x = x;
	_y = y;
}

// get position of playr using reference variable
void Player::getPosition(int &x, int &y){
	x = _x;
	y = _y;
}

int Player::attack(){
	static default_random_engine randomEngine(time(NULL));
	uniform_int_distribution<int> attackRoll(0, _attack);
	return attackRoll(randomEngine);
}

void Player::addExperience(int val){
	_experience += val;
	while (_experience > 50){
		_experience -= 50;
		_attack += 10;
		_defence += 10;
		_health += 10;
		_level++;
	}
}
int Player::takeDamage(int attack){
	attack -= _defence;
	if (attack > 0){
		_health -= attack;
		if (_health <= 0){
			return 1;
		}
	}
	return 0;
}